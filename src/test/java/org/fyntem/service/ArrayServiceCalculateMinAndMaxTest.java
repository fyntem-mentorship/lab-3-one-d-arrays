package org.fyntem.service;

import org.fyntem.service.ArrayService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ArrayServiceCalculateMinAndMaxTest {

    @Test
    public void testCalculateMinAndMaxElementsOfSubArrays1() {
        int[] incomeArray = new int[] {8, 4, -3, 5, 7, 1, 0};
        int[] resultArray = ArrayService.calculateMinAndMaxElementsOfSubArrays(incomeArray);

        assertArrayEquals(new int[] {-3, 5}, resultArray);
    }

    @Test
    public void testCalculateMinAndMaxElementsOfSubArrays2() {
        int[] incomeArray = new int[] {9,9,9,9,9,9,9};
        int[] resultArray = ArrayService.calculateMinAndMaxElementsOfSubArrays(incomeArray);

        assertArrayEquals(new int[] {9,9}, resultArray);
    }
    @Test
    public void testCalculateMinAndMaxElementsOfSubArrays3() {
        int[] incomeArray = new int[] {0};

        assertThrows(IllegalArgumentException.class, () -> ArrayService.calculateMinAndMaxElementsOfSubArrays(incomeArray));
    }

    @Test
    public void testCalculateMinAndMaxElementsOfSubArrays4() {
        int[] incomeArray = new int[] {5,1};
        int[] resultArray = ArrayService.calculateMinAndMaxElementsOfSubArrays(incomeArray);

        assertArrayEquals(new int[] {5,1}, resultArray);
    }
}
