package org.fyntem.service;

import org.fyntem.service.ArrayService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class ArrayServiceFindElementsInAnArrayThatAreClosestToTheRealNumberTest {

    @Test
    public void testFindElementsInAnArrayThatAreClosestToTheRealNumber1() {
        int[] incomeArray = new int[] {2,1,10,4,5,7};
        double realNumber = 8.17;
        int[] resultArray = ArrayService.findElementsInAnArrayThatAreClosestToTheRealNumber(incomeArray, realNumber);

        assertArrayEquals(new int[] {7,10}, resultArray);
    }

    @Test
    public void testFindElementsInAnArrayThatAreClosestToTheRealNumber2() {
        int[] incomeArray = new int[] {-2,1,-6,10,4,5,7};
        double realNumber = -3.56;
        int[] resultArray = ArrayService.findElementsInAnArrayThatAreClosestToTheRealNumber(incomeArray, realNumber);

        assertArrayEquals(new int[] {-6,-2}, resultArray);
    }

    @Test
    public void testFindElementsInAnArrayThatAreClosestToTheRealNumber3() {
        int[] incomeArray = new int[] {-5,1,-6,10,4,5,7};
        double realNumber = -5.8;
        int[] resultArray = ArrayService.findElementsInAnArrayThatAreClosestToTheRealNumber(incomeArray, realNumber);

        assertArrayEquals(new int[] {-6,-5}, resultArray);
    }

    @Test
    public void testFindElementsInAnArrayThatAreClosestToTheRealNumber4() {
        int[] incomeArray = new int[] {7,7,7,7};
        double realNumber = 8.1;
        int[] resultArray = ArrayService.findElementsInAnArrayThatAreClosestToTheRealNumber(incomeArray, realNumber);

        assertArrayEquals(new int[] {7,256}, resultArray);
    }

    @Test
    public void testFindElementsInAnArrayThatAreClosestToTheRealNumber5() {
        int[] incomeArray = new int[] {7,7,7,7};
        double realNumber = 6;
        int[] resultArray = ArrayService.findElementsInAnArrayThatAreClosestToTheRealNumber(incomeArray, realNumber);

        assertArrayEquals(new int[] {-256,7}, resultArray);
    }

    @Test
    public void testFindElementsInAnArrayThatAreClosestToTheRealNumber6() {
        int[] incomeArray = new int[] {0,0};
        double realNumber = 0;
        int[] resultArray = ArrayService.findElementsInAnArrayThatAreClosestToTheRealNumber(incomeArray, realNumber);

        assertArrayEquals(new int[] {0,0}, resultArray);
    }
}
