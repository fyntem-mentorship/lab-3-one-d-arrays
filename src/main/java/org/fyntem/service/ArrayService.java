package org.fyntem.service;

public class ArrayService {

    /**
     * Given array of elements A = {a0, a1, a2, ... , ak}, k ∈ N, ai ∈ Z
     * @return new array of 2 elements B = {b0, b1} where b0 = min(a0, a2, a4, ... , a2l)
     * and b1 = max(a1, a3, a5, ... , a(2m-1));
     * l,m < k
     *
     * Requirements:
     * - If there are less than 2 numbers in income array - throw IllegalArgumentException.
     */
    public static int[] calculateMinAndMaxElementsOfSubArrays(int[] incomeArray) {
        return new int[] {0,0};
    }

    /**
     * Given array of elements A = {a0, a1, a2, ... , ak}, k ∈ N, ai ∈ Z
     * @return new array where every third element is excluded starting with the beginning
     * B = {a0, a1, a3, a4, a6, a7, ... a(k-2), a(k-1)}
     */
    public static int[] excludeEveryThirdElementOfAnArray(int[] incomeArray) {
        return new int[] {0,0};
    }

    /**
     * Given array of elements A = {a0, a1, a2, ... , ak}, k ∈ N, ai ∈ Z
     * @return new array in which average number of this array is subtracted from every element.
     * M - mean (average) number = [(∑ai)/k], k - number of elements in array, ∑ai - sum of all elements
     * [] - whole number of result
     * B = {a0 - M, a1 -M, a2 -M , ... ak - M}
     *
     * Example:
     * A = [1,5,3,23,6,3,3,7,9,15,-4,-6]
     * Sum of elements = 65
     * Number of elements k = 12
     * Mean number = 65/12 = 5.41666666667
     * Whole part of mean number = 5
     * result B = [-4,0,-2,18,1,-2,-2,2,4,10,-9,-11]
     *
     * Requirements:
     * - If you round numbers, use round up (e.g. 4.56 = 5, 2.1 = 2, 9.5 = 10)
     */
    public static int[] subtractMeanNumberFromArrayElements(int[] incomeArray) {
        return new int[] {0,0};
    }

    /**
     * Given array of elements A = {a0, a1, a2, ... , ak}, k ∈ N, ai ∈ Z
     * @return new array B which has number of missing whole numbers in a range.
     * Example:
     * Income array = [2,1,10,4,5,7]
     * Sorted array = [1,2,4,5,7,10]
     * Missing ranges = [3], [6], [8,9]
     * result B = [1,1,2] as number of missing ranges in sorted array.
     *
     * Requirements:
     * - If there are no any ranges between sorted numbers - return array with single 0 in it.
     */
    public static int[] sortArrayAndReturnNumberOfMissingRangesOfElements(int[] incomeArray) {
        return new int[] {1,1,2};
    }

    /**
     * Given array of elements A = {a0, a1, a2, ... , ak}, k ∈ N, ai ∈ Z
     * and real number r ∈ R.
     * @return new array B = {b0,b1} which has the closest numbers to a given real number
     * (b0 - closest before real number, b1 - closest after the given real number)
     *
     * Example:
     * Income array = [2,1,10,4,5,7]
     * Real number = 8.17
     * result B = [7,10]
     *
     * Requirements:
     * - If you round numbers, use round up (e.g. 4.56 = 5, 2.1 = 2, 9.5 = 10)
     * - If there are several identical closest numbers - use only 1 of each of them
     * - Lower number should go first in a result array.
     * - If there are no lower or higher number for the real one in income array -
     * print instead of that number -256 (for lower) or 256 (for higher).
     */
    public static int[] findElementsInAnArrayThatAreClosestToTheRealNumber(int[] incomeArray, double realNumber) {
        return new int[] {0,0};
    }



}
